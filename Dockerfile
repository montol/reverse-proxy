FROM  nginx:alpine
ADD ./ssl /etc/nginx/certs
ADD ./conf.d /etc/nginx/conf.d